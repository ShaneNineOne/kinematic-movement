﻿using UnityEngine;
using System.Collections;

public class CreateObstacle : MonoBehaviour {

    public Transform obstacle;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire2"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Floor")//Prevent detection of player so they can't click on themselves
            {
                Instantiate(obstacle, hit.point, Quaternion.identity);
            }
        }
    }
}
