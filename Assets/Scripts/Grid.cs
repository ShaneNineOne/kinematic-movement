﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour
{
    /// <summary>
    /// Should we draw the grid in the editor?
    /// </summary>
    public bool drawGrid;

    public LayerMask unwalkableMask;
    public Vector2 gridWorldSize;
    public float nodeRadius;
    Node[,] grid;

    float nodeDiameter;
    int gridSizeX, gridSizeY;

    void Awake()
    {
        nodeDiameter = nodeRadius * 2;
        gridSizeX = Mathf.RoundToInt(gridWorldSize.x / nodeDiameter);
        gridSizeY = Mathf.RoundToInt(gridWorldSize.y / nodeDiameter);
        CreateGrid();
    }

    /// <summary>
    /// Creates the grid based on the size values set in the inspector.
    /// </summary>
    public void CreateGrid()
    {
        grid = new Node[gridSizeX, gridSizeY];
        Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x / 2 - Vector3.forward * gridWorldSize.y / 2;

        for (int x = 0; x < gridSizeX; x++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.forward * (y * nodeDiameter + nodeRadius);
                bool walkable = !(Physics.CheckSphere(worldPoint, nodeRadius, unwalkableMask));
                grid[x, y] = new Node(walkable, worldPoint, x, y);
            }
        }
    }

    /// <summary>
    /// Returns all of the neighbors, minus diagonal neigbors, of a node.
    /// </summary>
    /// <param name="node">The node to generate neighbors from.</param>
    /// <returns>A list of nodes containing all of the neighbors of the node that was passed in.</returns>
    public List<Node> GetNeighbors(Node node)
    {
        List<Node> neighbors = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if ((x == 0 && y == 0) || (x == -1 && y != 0) || (x == 1 && y != 0)) //Ignore the middle node (that we are expanding out from) or the diagnol nodes (at x=-1, y=1 or y=-1 and x=1, y=1 or y=-1)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                {
                    neighbors.Add(grid[checkX, checkY]);
                }
            }
        }

        return neighbors;
    }

    /// <summary>
    /// Returns a node within the grid that corresponds to the point of an object's position.
    /// </summary>
    /// <param name="worldPosition">The position we want to convert into a node on the grid.</param>
    /// <returns>The node on the grid that represents the position at worldPosition.</returns>
    public Node NodeFromWorldPoint(Vector3 worldPosition)
    {
        //We add our position to the worldposition.x and worldposition.z to counterat our A* object not being centered at (0, 0, 0). This effectively shifts the grid into its correct world space.
        //Also note: we use worldposition.z, not worldposition.y. This is due to us creating a 2D grid in 3D space, so our Y value is functionally our Z value.
        float percentX = ((worldPosition.x - transform.position.x) + gridWorldSize.x / 2) / gridWorldSize.x;
        float percentY = ((worldPosition.z - transform.position.z) + gridWorldSize.y / 2) / gridWorldSize.y;
        percentX = Mathf.Clamp01(percentX);
        percentY = Mathf.Clamp01(percentY);

        int x = Mathf.RoundToInt((gridSizeX - 1) * percentX);
        int y = Mathf.RoundToInt((gridSizeY - 1) * percentY);
        return grid[x, y];
    }

    /// <summary>
    /// This is used for two purposes: drawing the outline of the grid in the editor so we can visualize how big our grid will be, and - provided we have chosen to do so - drawing the grid in the editor.
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(gridWorldSize.x, 1, gridWorldSize.y));

        if (grid != null && drawGrid)
        {

            foreach (Node n in grid)
            {
                Gizmos.color = (n.walkable) ? new Color(1, 0, 1, 0.5f) : Color.red;
                Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter - .1f));
            }
        }
    }
}