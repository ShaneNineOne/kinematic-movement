﻿using UnityEngine;
using System.Collections;

public class Follower : MonoBehaviour {

    private Vector3 target;

    public Transform leader;

    public Transform fallback;

    public BasicAi leaderAI;

    public int position;

    /// <summary>
    /// Minimum distance before we stop moving towards our destination
    /// </summary>
    private float minDistance = 0.5f;

    public float turnSpeed = 1f;

    private bool goToFallback = false;

    /// <summary>
    /// How fast can we move?
    /// </summary>
    private float maxSpeed;

    public bool obstacleDetected = false;

    [SerializeField] private Rigidbody Rb;

    void Start ()
    {
        maxSpeed = 5f;
        leaderAI = leader.GetComponent<BasicAi>();
        findTargets();
        transform.position = target;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            goToFallback = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            goToFallback = false;
        }
    }

    // Update is called once per frame
    void Update () {

        if (!goToFallback)
        {
            findTargets();
        } else
        {
            findFallbacks();
        }
        
        
        if (transform.position != target)
        {
            Vector3 towards = target - transform.position;
            kinematicMovement(towards);
        }

        //Determine which position we are in. From here, we try to stay at a point relative to the leader at all times. We ignore the target ourselves, our position updates are purely in regard to the leader.
        //HINT: Start by using debug.drawLine to see where the positions are at all times. Don't move the other cubes until this looks right.
    }

    private void kinematicMovement(Vector3 direction)
    {
        //TODO: If we are detecting an object, just move forward, not toward the target. Once we are no longer detecting an object and have passed by the obstacle, lerp back to the target and move to it again.
        if (!(direction.magnitude < minDistance))
        {
            if (direction.magnitude > maxSpeed)
            {
                direction.Normalize();
            }
            direction *= maxSpeed;
            Rb.velocity = direction;
        }
    }

    private void findFallbacks()
    {
        if (position == 1 || position == 2)
        {
            target = ((Quaternion.Euler(0, 90, 0) * leader.transform.forward) * -1.5f) + transform.position;
            Debug.DrawLine(transform.position, target, Color.red);
        }
        else if (position == 3)
        {
            target = ((Quaternion.Euler(0, 90, 0) * leader.transform.forward) * 1.5f) + transform.position;
            Debug.DrawLine(transform.position, target, Color.red);
        }
    }

    private void findTargets()
    {
        if (position == 1)
        {
            target = ((Quaternion.Euler(0, 120, 0) * leader.transform.forward) * 1.5f) + leader.transform.position;//Define the target position... rotate relative to leader's forward vector, exend the vector, then add to leaders position to make it relative to his position.
            Debug.DrawLine(transform.position, target, Color.blue);
            Quaternion turnAngle = leader.rotation * Quaternion.Euler(0.0f, 45.0f, 0.0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, turnAngle, turnSpeed * Time.deltaTime);
        }
        else if (position == 2)
        {
            target = ((Quaternion.Euler(0, 150, 0) * leader.transform.forward) * 3f) + leader.transform.position;//Define the target position... rotate relative to leader's forward vector, exend the vector, then add to leaders position to make it relative to his position.
            Debug.DrawLine(transform.position, target, Color.cyan);
            Quaternion turnAngle = leader.rotation * Quaternion.Euler(0.0f, 180.0f, 0.0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, turnAngle, turnSpeed * Time.deltaTime);
        }
        else
        {
            target = ((Quaternion.Euler(0, 210, 0) * leader.transform.forward) * 1.5f) + leader.transform.position;//Define the target position... rotate relative to leader's forward vector, exend the vector, then add to leaders position to make it relative to his position.
            Debug.DrawLine(transform.position, target, Color.green);
            Quaternion turnAngle = leader.rotation * Quaternion.Euler(0.0f, 270.0f, 0.0f);
            transform.rotation = Quaternion.Lerp(transform.rotation, turnAngle, turnSpeed * Time.deltaTime);
        }
    }
}
