﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinding : MonoBehaviour
{
    //TODO: This entire script is in need of some cleaning up...
    [SerializeField]
    private Transform target;

    public bool pathFound = false;
    public int currentWaypoint = 0;

    private BasicAi leaderControl;


    /// <summary>
    /// Should we draw the path when it has been found? (FOR DEBUG PURPOSES ONLY)
    /// </summary>
    public bool drawPath;

    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance;

    public List<Node> path;

    Grid grid;

    void Awake()
    {
        leaderControl = GetComponent<BasicAi>();
        grid = FindObjectOfType<Grid>();
    }

    void Update()
    {
        if (pathFound)
        {
            //Break free of the path either if we have gotten to the end, or if we have detected the player.
            if (currentWaypoint >= (path.Count - 1))
            {
                pathFound = false;
                currentWaypoint = 0;
                Debug.Log(transform.gameObject.name + " has reached the end of its path.");
                //TODO: Implement in enemy controller a "search" state. At the end of each path, the monster will run a "search", essentially looking around to see if it can spot anything. After the search, generate a new path.
            }
            else {
                pathFoundAction();
            }
        }
    }

    /// <summary>
    /// The actions we take after we have found a path.
    /// </summary>
    void pathFoundAction()
    {
        if (drawPath)
        {
            drawCurrentPath();
        }

        Vector3 dir = (path[currentWaypoint].worldPosition - transform.position).normalized;
        leaderControl.kinematicMovement(dir);

        if (Vector3.Distance(transform.position, path[currentWaypoint].worldPosition) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }

    /// <summary>
    /// Finds a path via A* from our start position to our target position.
    /// </summary>
    /// <param name="startPos">The starting position of the AI.</param>
    /// <param name="targetPos">The target we are trying to pathfind our way to.</param>
    public void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        currentWaypoint = 0;
        grid.CreateGrid();
        path.Clear();

        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node targetNode = grid.NodeFromWorldPoint(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                {
                    if (openSet[i].hCost < node.hCost)
                        node = openSet[i];
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);

            if (node == targetNode)
            {
                //Debug.Log(transform.gameObject.name + " has found a path!");
                pathFound = true;
                RetracePath(startNode, targetNode);
                return;
            }

            foreach (Node neighbor in grid.GetNeighbors(node))
            {
                if (!neighbor.walkable || closedSet.Contains(neighbor))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbor);
                if (newCostToNeighbour < neighbor.gCost || !openSet.Contains(neighbor))
                {
                    neighbor.gCost = newCostToNeighbour;
                    neighbor.hCost = GetDistance(neighbor, targetNode);
                    neighbor.parent = node;

                    if (!openSet.Contains(neighbor))
                        openSet.Add(neighbor);
                }
            }
        }
    }

    void RetracePath(Node startNode, Node endNode)
    {
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
    }

    private void drawCurrentPath()
    {
        for (int x = 0; x < path.Count - 2; x++)
        {
            Debug.DrawLine(path[x].worldPosition, path[x + 1].worldPosition, Color.green);
        }
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }
}