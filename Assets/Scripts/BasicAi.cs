﻿using UnityEngine;
using System.Collections;

public class BasicAi : MonoBehaviour
{
    public Vector3 target;
    /// <summary>
    /// 
    /// </summary>
    public Vector3 turnDir;

    public Rigidbody Rb;

    public bool obstacleDetected;
    [SerializeField]
    private bool obstacleInFront;

    /// <summary>
    /// How long should our lerp take to complete?
    /// </summary>
    private float lerpTimeToComplete = 1f;
    /// <summary>
    /// Holds the starting time of each lerp (taken from Time.time)
    /// </summary>
    private float lerpStartTime;
    /// <summary>
    /// Are we lerping still?
    /// </summary>
    private bool lerping;

    /// <summary>
    /// Minimum distance before we stop moving towards our destination
    /// </summary>
    private int minDistance = 1;
    /// <summary>
    /// Has the player clicked to move?
    /// </summary>
    private bool fired = false;
    /// <summary>
    /// What is the maximum speed the player can move?
    /// </summary>
    private float maxSpeed;

    private float turnSpeed;

    private Pathfinding pathfinder;

    /// <summary>
    /// What is the last obstacle that we detected?
    /// </summary>
    [SerializeField]
    private GameObject lastHit;

    void Start()
    {
        pathfinder = GetComponent<Pathfinding>();
        maxSpeed = 5f;
        turnSpeed = 1f;
        obstacleDetected = false;
    }

    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit) && hit.transform.tag == "Floor")//Prevent detection of player so they can't click on themselves
            {
                fired = true;
                target = hit.point;
                lerping = true;
                lerpStartTime = Time.time;
                pathfinder.FindPath(transform.position, target);
            }
        }
    }

    public void kinematicMovement(Vector3 dir)
    {
        Debug.DrawLine(transform.position, pathfinder.path[pathfinder.currentWaypoint].worldPosition);

        rotateTowards(dir);

        if (dir.magnitude > maxSpeed)
        {
            dir.Normalize();
        }
        dir *= maxSpeed;

        Rb.velocity = dir;
    }

    public void rotateTowards(Vector3 dir)
    {
        if (Vector3.Dot(new Vector3(dir.x, transform.position.y, dir.z), transform.forward) < 1f)
        {
            Quaternion newRotation = Quaternion.LookRotation(dir);
            //We are only interested in the Y axis for AI's rotation, so we set the others to 0.
            newRotation.z = 0.0f;
            newRotation.x = 0.0f;
            transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, .2f);

        }
    }
}